/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "IntervaledTaskCPU.hpp"
#include <iostream>

#ifdef WIN32
int IntervaledTaskCPU::GetCPULoad()
{
return mTaskInterval*3;
}
#else
int IntervaledTaskCPU::GetCPULoad()
{
   int FileHandler;
   char FileBuffer[1024];
   float load;
   FileHandler = open("/proc/loadavg", O_RDONLY);
   if(FileHandler < 0) {
	  return -1; }
   read(FileHandler, FileBuffer, sizeof(FileBuffer) - 1);
   sscanf(FileBuffer, "%f", &load);
   close(FileHandler);
   return (int)(load * 100);
}
#endif
void IntervaledTaskCPU::DoTask()
{
std::cout << "Task Interval:" << mTaskInterval << " cpu=" << IntervaledTaskCPU::GetCPULoad() << "%" << std::endl;
}


