/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IntervaledTaskCPU_HPP
#define IntervaledTaskCPU_HPP


#include <boost/shared_ptr.hpp>
#include "IntervaledTask.hpp"
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */

class IntervaledTaskCPU : public IntervaledTask
{
public:
/*	IntervaledTaskCPU();
	~IntervaledTaskCPU();
	void UpdateFromShadow(int newInterval);*/
	void DoTask();
private:
	int GetCPULoad();
//	int mTaskInterval;
protected:
	//protected stuff
};

typedef boost::shared_ptr<IntervaledTask> IntervaledTaskPtr;

#endif
