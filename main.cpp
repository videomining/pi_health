#include "IntervaledTaskTemperature.hpp"
#include "IntervaledTaskCPU.hpp"
#include "CountDownTimer.hpp"
//#include "QueuedThreadPool.hpp"

int main()
{
//	boost::shared_ptr<QueuedThreadPool> myLocalQue(new QueuedThreadPool(4));

	std::cout <<"Create Tasks" << std::endl;
	std::list <IntervaledTaskPtr> metric_list;
	std::list <CountDownTimerPtr> callback_list;
	for(int i=1;i<4;i++){
      if(i%2==0){
		  metric_list.push_back(IntervaledTaskPtr(new IntervaledTaskTemperature()));
	  }
	  else{
		  metric_list.push_back(IntervaledTaskPtr(new IntervaledTaskCPU()));
	  }
	}
	int i=0;
	for(std::list<IntervaledTaskPtr>::iterator list_iter = metric_list.begin(); 
	    list_iter != metric_list.end(); list_iter++){
		i++;
		  callback_list.push_back(CountDownTimerPtr(new CountDownTimer(i,(*list_iter))));
	}
	//Create a Temperature Task
//	CountDownTimer* MyTimer= new CountDownTimer(1,IntervaledTaskPtr(new IntervaledTaskTemperature()));
	//Create a CPU Task
//	CountDownTimer* MyTimer2= new CountDownTimer(3,IntervaledTaskPtr(new IntervaledTaskCPU()));
	//Create a second CPU Task
//	CountDownTimer* MyTimer3= new CountDownTimer(4,IntervaledTaskPtr(new IntervaledTaskCPU()));
	//Create a Temperature Task
	//boost::shared_ptr<NewCountDownTimer> MyTimer4(new NewCountDownTimer(1,IntervaledTaskPtr(new IntervaledTaskTemperature())));
	/*
	for(int i=1;i<8;i++){
		myLocalQue->push(boost::shared_ptr<NewCountDownTimer>(new NewCountDownTimer(i,IntervaledTaskPtr(new IntervaledTaskTemperature()))));
		}
	*/
    std::cout <<"Tasks Created" << std::endl;

	//pause the main thread
	//later we will use this thread to do work while the other tasks work
#ifdef WIN32
	Sleep(15000);
#else
	sleep(15);
#endif
	for(std::list<IntervaledTaskPtr>::iterator list_iter = metric_list.begin(); 
	    list_iter != metric_list.end(); list_iter++){
			i++;
			list_iter->get()->updateFromShadow(i*2);
	}

#ifdef WIN32
	Sleep(25000);
#else
	sleep(25);
#endif	// Sleep is a Windows function. For Unix, look into using nanosleep (POSIX) or usleep (BSD; deprecated)
	std::cout <<"Delete Tasks" << std::endl;
	for(std::list<CountDownTimerPtr>::iterator list_iter = callback_list.begin(); 
	    list_iter != callback_list.end(); list_iter++){
	list_iter->reset();
	}
	for(std::list<IntervaledTaskPtr>::iterator list_iter = metric_list.begin(); 
	    list_iter != metric_list.end(); list_iter++){
	list_iter->reset();
	}
	std::cout <<"Tasks Deleted" << std::endl;
#ifdef WIN32
	Sleep(3000);
#else
	sleep(3);
#endif

	//Delete our Temperature Task
//	delete MyTimer;
	//Delete our first CPU Task
//	delete MyTimer2;
	//Delete our second CPU Task
//	delete MyTimer3;
//	myLocalQue.reset();

	std::cout <<"Main Thread Exiting" << std::endl;

	return 0;
}
