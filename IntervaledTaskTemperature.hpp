/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/

#ifndef IntervaledTaskTemperature_HPP
#define IntervaledTaskTemperature_HPP

#include <boost/shared_ptr.hpp>
#include "IntervaledTask.hpp"

class IntervaledTaskTemperature : public IntervaledTask
{
public:
/*	IntervaledTaskTemperature();
	~IntervaledTaskTemperature();
	void UpdateFromShadow(int newInterval);*/
	void DoTask();
private:
//	int mTaskInterval;
protected:
	//protected stuff
};

typedef boost::shared_ptr<IntervaledTask> IntervaledTaskPtr;

#endif
