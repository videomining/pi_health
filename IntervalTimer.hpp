/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
//Declare sentry ifndef to stop reinclude when header is reused
#ifndef IntervalTimer_HPP
#define IntervalTimer_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/timer/timer.hpp>
#include "IntervaledTask.hpp"
#include "CountDownTimer.hpp"

//namespace healthapp 
//{
class IntervalTimer : public CountDownTimer
{
public:
	std::list <IntervaledTaskPtr> callback_list;
	IntervalTimer(int interval);
	~IntervalTimer();
	void OnTimerExpire();
	// Add a task to execute when timer expires
	void AddTask(IntervaledTaskPtr taskPtr);

	// remove a task from the execution list
	int RemoveCallBack(IntervaledTaskPtr taskPtr);

};

//close namespace
// }

//close Sentry
#endif
