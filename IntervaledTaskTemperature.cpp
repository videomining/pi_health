/****************************************************************************/
/*                    (C) Copyright VideoMining Corporation                 */
/*                           All Rights Reserved                            */
/****************************************************************************/
#include "IntervaledTaskTemperature.hpp"
#include <iostream>

void IntervaledTaskTemperature::DoTask()
{
#ifdef WIN32
std::cout << "Task Interval:" << mTaskInterval << " Temp=" << mTaskInterval*5 << std::endl;
#else
	system("/opt/vc/bin/vcgencmd measure_temp");
#endif
}


