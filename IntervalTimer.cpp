#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/timer/timer.hpp>
#include "IntervalTimer.hpp"


IntervalTimer::IntervalTimer(int interval)
	:CountDownTimer(interval)
{
	boost::mutex::scoped_lock scoped_lock(mTimerMutex);
//	ExpCallback = callback(callbackFunc);
/*	mTimer.init();
	if (mTimer == NULL)
	{
		throw std::exception("Could not create timer");
	}
	//Set ExpCallback of timer (timer, IntervalOnTimerExpire);
*/
}

	IntervalTimer::~IntervalTimer()
{
	boost::mutex::scoped_lock scoped_lock(mTimerMutex);
	//mTimer.stop();
	callback_list.clear();
}

void 
IntervalTimer::AddTask(IntervaledTaskPtr taskPtr)
{
boost::mutex::scoped_lock lock(mTimerMutex);
bool wasEmpty = callback_list.empty();
callback_list.push_back(taskPtr);
if(wasEmpty){
	//start the timer
	//mTimer.start();
	}
};

int 
IntervalTimer::RemoveCallBack(IntervaledTaskPtr taskPtr)
{
boost::mutex::scoped_lock lock(mTimerMutex);
std::list<IntervaledTaskPtr>:: iterator targetPtr = std::find(callback_list.begin(), callback_list.end(), taskPtr);
callback_list.erase(targetPtr);
int newLength = callback_list.size();
return newLength;
};

void 
IntervalTimer::OnTimerExpire()
{
//mTimer.stop();
boost::mutex::scoped_lock lock(mTimerMutex);

// Post Each IntetvalTask Callback to Thread Pool Queue
//Is a reset of the interval necessary here?
//mTimer.start();
};

